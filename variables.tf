variable "region" {
  description = "Definindo a região da cloud"
  default     = "us-central1"
}

variable "zone" {
  description = "Definindo a zona da cloud"
  default     = "us-central1-c"
}

variable "project" {
  description = "Nome do projeto na cloud"
  default     = "projeto-iesp"
}

variable "cluster_name" {
  description = "Nome do Cluster"
  default     = "k8s-gke-uniesp"
}

variable "nodes_count" {
  description = "Quantidade de nos"
  default     = 1
}

variable "external_tags" {
  description = "Tags dos recursos"
  default = ["xyz"]
}