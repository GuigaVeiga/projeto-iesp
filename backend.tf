terraform {
  backend "gcs" {
    bucket      = "projeto-gke-uniesp"
    prefix      = "dev/state"
    credentials = "user.json"
  }
}