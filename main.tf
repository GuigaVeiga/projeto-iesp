module "gke" {
  source        = "./modules/gke"
  credentials   = file("user.json")
  project       = var.project
  region        = var.region
  zone          = var.zone
  cluster_name  = var.cluster_name
  nodes_count   = var.nodes_count
  external_tags = var.external_tags
}